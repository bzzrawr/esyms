/**
 *
 * HomePage
 *
 */
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Pagination,
  Rating,
  Typography,
} from '@mui/material';
import React, { memo, useEffect, useState } from 'react';
import { amtStr } from 'utils/functions/numberFormat';
import usePagination from 'utils/hooks/usePagination';
import { styled } from '@mui/material/styles';

const StyledPagination = styled(Pagination)(({ theme }) => ({
  color: 'inherit',
  '& .MuiPagination-ul': {
    placeContent: 'center',
  },
}));

interface Props {}

export const HomePage = memo((props: Props) => {
  let [page, setPage] = useState(1);
  const [itemList, setItemList] = useState([]);
  const PER_PAGE = 10;

  useEffect(() => {
    fetch(
      `https://staging-backend.esyms-api.com/esyms/website/newArrival?page=1&limit=999`,
    )
      .then(res => res.json())
      .then(data => {
        let { docs } = data.results;
        setItemList(docs?.filter(e => e.routeOfAdministration === 'INTERNAL'));
      });
  }, []);
  const count = Math.ceil(itemList?.length / PER_PAGE);
  const _DATA = usePagination(itemList, PER_PAGE);

  const handleChange = (e, p) => {
    setPage(p);
    _DATA.jump(p);
  };
  console.log('check', _DATA.currentData());
  return (
    <Grid container sx={{ textAlign: 'center' }}>
      <Grid item xs={12}>
        <Typography paragraph>
          Assessment from ESYM. Search something on header
        </Typography>
      </Grid>
      <Grid item xs={12} sx={{ paddingBottom: '10px' }}>
        <Grid container spacing={{ xs: 1.5, md: 1.5 }}>
          {_DATA.currentData().map((data, index) => {
            return (
              <Grid key={index} item xs={6} sm={4} md={3} lg={2}>
                <CardActionArea>
                  <Card sx={{ height: '336px' }}>
                    <CardMedia
                      component="div"
                      sx={{
                        height: '180px',
                        textAlign: 'center',
                      }}
                      children={
                        <img
                          src={`https://cdn.esyms.com/${data?.img[0]?.src}`}
                          style={{
                            height: '100%',
                          }}
                          alt="green iguana"
                        />
                      }
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="div"
                        fontSize="13px"
                        sx={{
                          overflowWrap: 'break-word',
                          whiteSpace: 'normal',
                          overflow: 'hidden',
                          display: '-webkit-box',
                          textOverflow: 'ellipsis',
                          WebkitBoxOrient: 'vertical',
                          WebkitLineClamp: 2,
                          marginBottom: '2.35em',
                        }}
                      >
                        {data?.name?.en}
                      </Typography>
                      <Grid container sx={{ textAlign: 'end' }}>
                        <Grid item xs={12}>
                          <Rating
                            name="read-only"
                            value={data?.rating}
                            readOnly
                            sx={{ fontSize: '12px' }}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <Typography
                            fontSize="0.75rem"
                            component="span"
                            sx={{ color: 'rgb(31, 169, 142)' }}
                          >
                            RM
                          </Typography>
                          <Typography
                            component="span"
                            fontSize="1rem"
                            sx={{ color: 'rgb(31, 169, 142)' }}
                          >
                            {amtStr(data?.price)}
                          </Typography>
                        </Grid>
                      </Grid>
                    </CardContent>
                  </Card>
                </CardActionArea>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <div style={{ placeContent: 'center' }}>
          <StyledPagination
            count={count}
            size="large"
            page={page}
            variant="outlined"
            shape="rounded"
            onChange={handleChange}
          />
        </div>
      </Grid>
    </Grid>
  );
});
