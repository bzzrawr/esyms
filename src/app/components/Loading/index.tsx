import { Backdrop, CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';

export default function Loading({ isLoading }) {
  const [open, setOpen] = useState(false);
  useEffect(() => {
    setOpen(true);
  }, [open]);
  if (isLoading) {
    return (
      <Backdrop
        open={open}
        transitionDuration={2000}
        sx={{
          color: '#fff',
          zIndex: theme => theme.zIndex.drawer + 1,
        }}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    );
  } else return null;
}
