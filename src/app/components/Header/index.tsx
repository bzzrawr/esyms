/**
 *
 * Header
 *
 */
import React, { memo } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import HomeIcon from '@mui/icons-material/Home';
import SearchIcon from '@mui/icons-material/Search';
import { useHeaderSlice } from './slice';
import { useDispatch, useSelector } from 'react-redux';
import { selectHeader } from './slice/selectors';
import { DrawerButton } from './DrawerButton';
import { SearchHeader } from './SearchHeader';

interface Props {
  header: {
    drawerWidth: number;
    title: string;
    navigate?(): any;
  };
}

export const Header = memo((props: Props) => {
  const { header } = props;
  const dispatch = useDispatch();
  const { actions } = useHeaderSlice();
  const { searchOnFocus } = useSelector(selectHeader);

  const onFocusEvent = () => {
    dispatch(actions.setSearchOnFocus({ searchOnFocus: !searchOnFocus }));
  };
  const onClickHome = () => {
    window.open('/', '_self');
  };
  return (
    <AppBar
      position="fixed"
      sx={{
        width: { sm: `calc(100% - ${header.drawerWidth}px)` },
        ml: { sm: `${header.drawerWidth}px` },
      }}
    >
      <Toolbar>
        <DrawerButton />
        <Typography
          variant="h6"
          component="div"
          sx={{
            flexGrow: 1,
            textAlign: { xs: 'center', sm: 'left' },
            display: { xs: !searchOnFocus ? 'block' : 'none', sm: 'block' },
          }}
        >
          {header.title}
        </Typography>
        {!searchOnFocus && (
          <IconButton
            size="large"
            aria-label="home button"
            aria-controls="menu-appbar"
            aria-haspopup="false"
            onClick={onFocusEvent}
            color="inherit"
          >
            <SearchIcon />
          </IconButton>
        )}
        <SearchHeader onFocus={searchOnFocus} onBlur={onFocusEvent} />
        <IconButton
          size="large"
          aria-label="home button"
          aria-controls="menu-appbar"
          aria-haspopup="false"
          onClick={onClickHome}
          color="inherit"
        >
          <HomeIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
});
