import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.searchHeader || initialState;

export const selectSearchHeader = createSelector([selectSlice], state => state);
