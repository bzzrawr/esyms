import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer } from 'utils/redux-injectors';
import { SearchHeaderState } from './types';

export const initialState: SearchHeaderState = {
  searchItem: '',
};

const slice = createSlice({
  name: 'searchHeader',
  initialState,
  reducers: {
    setSearchItem(state, action: PayloadAction<any>) {
      state.searchItem = action.payload.searchItem;
    },
  },
});

export const { actions: searchHeaderActions } = slice;

export const useSearchHeaderSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useSearchHeaderSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
