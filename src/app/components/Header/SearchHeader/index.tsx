/**
 *
 * SearchHeader
 *
 */
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useSearchHeaderSlice } from './slice';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import { useNavigate } from 'react-router-dom';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

interface Props {
  onFocus: boolean;
  onBlur: any;
}

export function SearchHeader({ onFocus, onBlur }: Props) {
  const dispatch = useDispatch();
  const { actions } = useSearchHeaderSlice();
  const history = useNavigate();

  const handleSearchItem = e => {
    let searchItem = e.target.value.toLowerCase();
    if (e.keyCode === 13) {
      dispatch(actions.setSearchItem({ searchItem }));
      history(`/search/${searchItem}`);
    }
  };

  if (onFocus) {
    return (
      <Search>
        <SearchIconWrapper>
          <SearchIcon />
        </SearchIconWrapper>
        <StyledInputBase
          placeholder="Search…"
          autoFocus
          inputProps={{ 'aria-label': 'search' }}
          // onChange={handleSearchItem}
          onBlur={onBlur}
          onKeyDown={handleSearchItem}
        />
      </Search>
    );
  } else return null;
}
