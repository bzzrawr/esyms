import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.header || initialState;

export const selectHeader = createSelector([selectSlice], state => state);
