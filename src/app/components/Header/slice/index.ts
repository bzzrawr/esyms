import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer } from 'utils/redux-injectors';
import { HeaderState } from './types';

export const initialState: HeaderState = {
  searchOnFocus: false,
};

const slice = createSlice({
  name: 'header',
  initialState,
  reducers: {
    setSearchOnFocus(state, action: PayloadAction<any>) {
      state.searchOnFocus = action.payload.searchOnFocus;
    },
  },
});

export const { actions: headerActions } = slice;

export const useHeaderSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useHeaderSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
