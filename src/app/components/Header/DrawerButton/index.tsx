/**
 *
 * DrawerButton
 *
 */
import { IconButton } from '@mui/material';
import * as React from 'react';
import MenuIcon from '@mui/icons-material/Menu';
import { useDispatch, useSelector } from 'react-redux';
import { selectDrawerButton } from './slice/selectors';
import { useDrawerButtonSlice } from './slice';

interface Props {}

export function DrawerButton(props: Props) {
  const dispatch = useDispatch();
  const { actions } = useDrawerButtonSlice();

  const { drawerOpen } = useSelector(selectDrawerButton);
  const handleDrawerToggle = () => {
    dispatch(actions.setDrawerOpen({ drawerOpen: !drawerOpen }));
  };

  return (
    <IconButton
      color="inherit"
      aria-label="open drawer"
      edge="start"
      onClick={handleDrawerToggle}
      sx={{ mr: 2, display: { sm: 'none' } }}
    >
      <MenuIcon />
    </IconButton>
  );
}
