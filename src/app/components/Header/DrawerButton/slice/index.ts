import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer } from 'utils/redux-injectors';
import { DrawerButtonState } from './types';

export const initialState: DrawerButtonState = {
  drawerOpen: false,
};

const slice = createSlice({
  name: 'drawerButton',
  initialState,
  reducers: {
    setDrawerOpen(state, action: PayloadAction<any>) {
      state.drawerOpen = action.payload.drawerOpen;
    },
  },
});

export const { actions: drawerButtonActions } = slice;

export const useDrawerButtonSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useDrawerButtonSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
