/* --- STATE --- */
export interface DrawerButtonState {
  drawerOpen: boolean;
}
