import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.drawerButton || initialState;

export const selectDrawerButton = createSelector([selectSlice], state => state);
