import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.itemList || initialState;

export const selectItemList = createSelector([selectSlice], state => state);
