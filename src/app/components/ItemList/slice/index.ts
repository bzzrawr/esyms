import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer } from 'utils/redux-injectors';
import { ItemListState } from './types';

export const initialState: ItemListState = {
  items: [],
};

const slice = createSlice({
  name: 'itemList',
  initialState,
  reducers: {
    updateItems(state, action: PayloadAction<any>) {
      state.items.push(...action.payload.items);
    },
    resetItems(state) {
      state.items = [];
    },
  },
});

export const { actions: itemListActions } = slice;

export const useItemListSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useItemListSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
