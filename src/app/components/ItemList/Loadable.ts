/**
 *
 * Asynchronously loads the component for ItemList
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ItemList = lazyLoad(
  () => import('./index'),
  module => module.ItemList,
);
