/**
 *
 * ItemList
 *
 */
import React, { memo, useEffect, useState } from 'react';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, Rating } from '@mui/material';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useDispatch, useSelector } from 'react-redux';
import { useItemListSlice } from './slice';
import { selectItemList } from './slice/selectors';
import { amtStr } from 'utils/functions/numberFormat';
import { useParams } from 'react-router-dom';

interface Props {}

export const ItemList = memo((props: Props) => {
  const dispatch = useDispatch();
  const { searchItem } = useParams();
  const { actions } = useItemListSlice();
  const { items } = useSelector(selectItemList);
  const [page, setPage] = useState(1);
  const fetchMoreData = tp => {
    let tempPage = tp;
    fetch(
      `https://staging-backend.esyms-api.com/esyms/website/product/front-condition?name=${searchItem}&page=${tempPage}`,
    )
      .then(res => res.json())
      .then(data => {
        let { docs } = data.results;
        dispatch(
          actions.updateItems({
            items: docs,
          }),
        );
      });
    setPage(tempPage + 1);
  };

  useEffect(() => {
    dispatch(actions.resetItems());
    fetchMoreData(1);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchItem]);

  return (
    <InfiniteScroll
      dataLength={items?.length}
      hasMore={false}
      next={() => fetchMoreData(page)}
      loader={<h4>Loading...</h4>}
    >
      <Grid container spacing={{ xs: 1.5, md: 1.5 }}>
        {items
          ?.filter(e => e.routeOfAdministration === 'INTERNAL')
          .map((data, index) => {
            return (
              <Grid key={index} item xs={6} sm={4} md={3} lg={2}>
                <CardActionArea>
                  <Card sx={{ height: '336px' }}>
                    <CardMedia
                      component="div"
                      sx={{
                        height: '180px',
                        textAlign: 'center',
                      }}
                      children={
                        <img
                          src={`https://cdn.esyms.com/${data?.img[0]?.src}`}
                          style={{
                            height: '100%',
                          }}
                          alt="green iguana"
                        />
                      }
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="div"
                        fontSize="13px"
                        sx={{
                          overflowWrap: 'break-word',
                          whiteSpace: 'normal',
                          overflow: 'hidden',
                          display: '-webkit-box',
                          textOverflow: 'ellipsis',
                          WebkitBoxOrient: 'vertical',
                          WebkitLineClamp: 2,
                          marginBottom: '2.35em',
                        }}
                      >
                        {data?.name?.en}
                      </Typography>
                      <Grid container sx={{ textAlign: 'end' }}>
                        <Grid item xs={12}>
                          <Rating
                            name="read-only"
                            value={5}
                            readOnly
                            sx={{ fontSize: '12px' }}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <Typography
                            fontSize="0.75rem"
                            component="span"
                            sx={{ color: 'rgb(31, 169, 142)' }}
                          >
                            RM
                          </Typography>
                          <Typography
                            component="span"
                            fontSize="1rem"
                            sx={{ color: 'rgb(31, 169, 142)' }}
                          >
                            {amtStr(data?.price)}
                          </Typography>
                        </Grid>
                      </Grid>
                    </CardContent>
                  </Card>
                </CardActionArea>
              </Grid>
            );
          })}
        <Grid xs={12} item sx={{ textAlign: 'center' }}>
          <Button
            variant="contained"
            sx={{ backgroundColor: 'rgb(109, 200, 191)' }}
            onClick={() => fetchMoreData(page)}
          >
            See More
          </Button>
        </Grid>
      </Grid>
    </InfiniteScroll>
  );
});
