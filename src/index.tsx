import * as React from 'react';
import { createRoot } from 'react-dom/client';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import App from './App';
import theme from './theme';
import { Provider } from 'react-redux';
import { configureAppStore } from 'store/configureStore';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement!);
const store = configureAppStore();

root.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <React.StrictMode>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <App />
      </React.StrictMode>
    </ThemeProvider>
  </Provider>,
);
