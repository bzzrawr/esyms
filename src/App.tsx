import { Toolbar } from '@mui/material';
import Box from '@mui/material/Box';
import { Header } from 'app/components/Header';
import { ItemList } from 'app/components/ItemList';
import { HomePage } from 'app/pages/HomePage/Loadable';
import * as React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

export default function App() {
  return (
    <BrowserRouter>
      <Box sx={{ display: 'flex', flexGrow: 1 }}>
        <Header
          header={{
            title: 'ESYMS',
            drawerWidth: 0,
          }}
        />
        <Box component="main" sx={{ flexGrow: 1, p: 1 }}>
          <Toolbar />
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path={`/search/:searchItem`} element={<ItemList />} />
          </Routes>
        </Box>
      </Box>
    </BrowserRouter>
  );
}
