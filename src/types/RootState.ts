import { HomePageState } from 'app/pages/HomePage/slice/types';
import { HeaderState } from 'app/components/Header/slice/types';
import { DrawerButtonState } from 'app/components/Header/DrawerButton/slice/types';
import { SearchHeaderState } from 'app/components/Header/SearchHeader/slice/types';
import { ItemListState } from 'app/components/ItemList/slice/types';
// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/* 
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/
export interface RootState {
  homePage?: HomePageState;
  header?: HeaderState;
  drawerButton?: DrawerButtonState;
  searchHeader?: SearchHeaderState;
  itemList?: ItemListState;
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
}
