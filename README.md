# ESYMS Assessment

## How to use

Clone the repo

<!-- #default-branch-switch -->

```sh
git clone https://gitlab.com/bzzrawr/esyms.git
```

Install it and run:

```sh
npm install or yarn
npm start or yarn start
```

Sample Apps:

```sh
https://esyms.testplayground.app/
```
